<?php

namespace App\Repositories;

use App\Models\author;
use App\Repositories\BaseRepository;

/**
 * Class authorRepository
 * @package App\Repositories
 * @version July 28, 2019, 7:40 pm UTC
*/

class authorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'bio',
        'image'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return author::class;
    }
}
